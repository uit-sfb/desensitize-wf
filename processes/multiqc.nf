process Multiqc {

  publishDir "output/multiqc"
  container 'hunt:latest'

  input:
  path before
  path screened
  path after

  output:
  path 'multiqc'

  shell:
  '''
  multiqc --outdir multiqc !{before} !{screened} !{after}
  '''
}
