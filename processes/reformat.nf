process pe_Reformat {

  echo true
  container 'hunt:latest'

  input:
  path r1, stageAs: 'in/*'
  path r2, stageAs: 'in/*'

  shell:
  '''
  reformat.sh in=!{r1} in2=!{r2} vpair
  '''
}
