process pe_First_Fastqc {

  echo true
  container 'hunt:latest'

  input:
  path r1
  path r2

  output:
  path "pre_screen/*_fastqc.{zip,html}", emit: fastqc_results

  shell:
  '''
  mkdir pre_screen
  fastqc -t !{params.cpu} -o pre_screen !{r1} !{r2}
  '''
}

process pe_Second_Fastqc {

  echo true
  container 'hunt:latest'

  input:
  path r1
  path r2

  output:
  path "post_screen/*_fastqc.{zip,html}", emit: fastqc_results

  shell:
  '''
  mkdir post_screen
  fastqc -t !{params.cpu} -o post_screen !{r1} !{r2}
  '''
}

process se_First_Fastqc {

  echo true
  container 'hunt:latest'

  input:
  path r1

  output:
  path "pre_screen/*_fastqc.{zip,html}", emit: fastqc_results

  shell:
  '''
  mkdir pre_screen
  fastqc -t !{params.cpu} -o pre_screen !{r1}
  '''
}

process se_Second_Fastqc {

  echo true
  container 'hunt:latest'

  input:
  path r1

  output:
  path "post_screen/*_fastqc.{zip,html}", emit: fastqc_results

  shell:
  '''
  mkdir post_screen
  fastqc -t !{params.cpu} -o post_screen !{r1}
  '''
}
