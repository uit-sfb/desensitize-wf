process pe_Fastqscreen {

  container 'hunt:latest'

  input:
  path r1, stageAs: 'in/*'
  path r2, stageAs: 'in/*'

  output:
  path "screened/*_screen.{txt,html}", emit: screened
  path "screened/${r1.getSimpleName()}.tagged_filter.fastq.gz", emit: screened_r1
  path "screened/${r2.getSimpleName()}.tagged_filter.fastq.gz", emit: screened_r2

  shell:
  '''
  fastq_screen --outdir screened --nohits !{r1} !{r2} --threads !{params.cpu} --conf /resources/fastq_screen.conf
  '''
}

process se_Fastqscreen {

  container 'hunt:latest'

  input:
  path r1, stageAs: 'in/*'

  output:
  path "screened/*_screen.{txt,html}", emit: screened
  path "screened/${r1.getSimpleName()}.tagged_filter.fastq.gz", emit: screened_r1

  shell:
  '''
  fastq_screen --outdir screened --nohits !{r1} --threads !{params.cpu} --conf /resources/fastq_screen.conf
  '''
}
