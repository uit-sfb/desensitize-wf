#!/usr/bin/env nextflow
// DSL 2 support and thread configuration. Default is 4 threads, provide --cpu on command line to change it. r2 defaults to null to supress warnings
nextflow.enable.dsl=2
params.cpu=4
params.r2=null

include {Bam2fq} from './processes/samtools.nf'

workflow{
  bam = params.bam.tokenize(',')
  bam = Channel.fromPath(bam).flatMap{files(it)}
  Bam2fq(bam)
}
