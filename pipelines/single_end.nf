// Include modules for each tool
include {se_First_Fastqc; se_Second_Fastqc} from '../processes/fastqc.nf'
include {se_Rename} from '../processes/rename.nf'
include {se_Fastqscreen} from '../processes/fastqscreen.nf'
include {Multiqc} from '../processes/multiqc.nf'

workflow Single_end {
  take:
    r1

  main:
    se_First_Fastqc(r1)
    se_Fastqscreen(r1)
    se_Rename(se_Fastqscreen.out.screened_r1)
    se_Second_Fastqc(se_Rename.out.renamed_r1)
    Multiqc(se_First_Fastqc.out.fastqc_results.collect(), se_Fastqscreen.out.screened.collect(), se_Second_Fastqc.out.fastqc_results.collect())
}
