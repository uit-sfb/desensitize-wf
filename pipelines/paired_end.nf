// Include modules for each tool
include {pe_Reformat} from '../processes/reformat.nf'
include {pe_First_Fastqc; pe_Second_Fastqc} from '../processes/fastqc.nf'
include {pe_Fastqscreen} from '../processes/fastqscreen.nf'
include {pe_Repair} from '../processes/repair.nf'
include {Multiqc} from '../processes/multiqc.nf'

workflow Paired_end {
  take:
    r1
    r2

  main:
    pe_Reformat(r1, r2)
    pe_First_Fastqc(r1, r2)
    pe_Fastqscreen(r1, r2)
    pe_Repair(pe_Fastqscreen.out.screened_r1, pe_Fastqscreen.out.screened_r2)
    pe_Second_Fastqc(pe_Repair.out.synced_r1, pe_Repair.out.synced_r2)
    //all = pe_First_Fastqc.out.dir.collect()
    //pe_First_Fastqc.out.dir.collect().view()
    //pe_Fastqscreen.out.dir.collect().view()
    //pe_Second_Fastqc.out.dir.collect().view()
    //Channel.from(pe_First_Fastqc.out.dir.collect(), pe_Fastqscreen.out.dir.collect(), pe_Second_Fastqc.out.dir.collect()).collect().view()
    //Channel.from(1,2,3).collect().view()
    Multiqc(pe_First_Fastqc.out.fastqc_results.collect(), pe_Fastqscreen.out.screened.collect(), pe_Second_Fastqc.out.fastqc_results.collect())
    //Multiqc(fastqc_results)
}
