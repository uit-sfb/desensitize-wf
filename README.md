## Hunt desensitize-wf
Nextflow/docker workflow to remove sensitive human DNA from metagenomic samples
#### Workflow
```mermaid
graph LR; reformat.sh-->PreFastQC; PreFastQC-->Fastq_screen; Fastq_screen-->repair.sh; repair.sh-->PostFastQC; PostFastQC-->MultiQC; 
```

#### Container: Sparse software/ref overview*
| Tool/pipeline |  Version  | Note |
|----------|:-------------:|------:|
| BB-map | 38.86 | reformat.sh/repair.sh |
| Fastqc | 0.11.9 | - |
| Fastq_screen | 0.14.0 | Ref: hg38 (GRCh38) |
| Multiqc | 1.9 | - |

(*excluding  dependencies)
## How-to
Desensitize-wf needs the following dependencies to run:
- Docker
- Nextflow (>= 20.07.1)

#### Installation:  
Clone this repository:  
``git clone  https://gitlab.com/uit-sfb/desensitize-wf.git``  

Navigate to the cloned repository:  
``cd desensitize-wf``  

Build docker container  
(tag needs to be "hunt" as this is referred to by workflows/modules. Total size is roughly 10GB):  
``docker build -t hunt .``  

#### Usage:  
##### Filename criteria:  
- No spaces, no commas and no dots (except .fastq.gz) in inputfilenames  
- fastq.gz file extension is required at this time  

##### Execution:  
The main workflow has two parameters, --r1 and --r2.  

To run in **single-end mode**, supply ONLY the --r1 paramter like so:  
``nextflow desensitize.nf --r1 <myfile.fastq.gz>``  
For multiple single-end samples, separate with a comma, like so:  
``nextflow desensitize.nf --r1 <myfile1.fastq.gz>,<myfile2.fastq.gz>,<myfile3.fastq.gz>,<myfile4.fastq.gz>``  

To run in **paired-end mode**, supply BOTH the --r1 and --r2 parameters, like so:  
``nextflow desensitize.nf --r1 <r1.fastq.gz> --r2 <r2.fastq.gz>``  

For multiple paired-end samples, separate with a comma, like so:  
``nextflow desensitize.nf --r1 <a_r1.fastq.gz>,<b_r1.fastq.gz> --r2 <a_r2.fastq.gz>,<b_r2.fastq.gz>``  
(Make sure files are ordered, so that the first file in --r1 corresponds to the first file in --r2 and so forth)  

Add the optional parameter --cpu to set cpu usage (default: 4)  

Results will be present in the output/ folder, and includes hg38-screened and synced fastq files as well as a QC-report from multiqc